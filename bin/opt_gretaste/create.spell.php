<?php

$data = file('/opt/gretaste/gretaste.spelling.dict', FILE_SKIP_EMPTY_LINES|FILE_IGNORE_NEW_LINES);

$string = file_get_contents("/opt/gretaste/dictionary.general.json");
$d = json_decode($string, true);
foreach($d as $e) {
	$data[]=trim($e['source']);
}

$string = file_get_contents("/opt/gretaste/dictionary.json");
$d = json_decode($string, true);
foreach($d as $e) {
	foreach($e['labels'] as $l) {
		if($l['lang'] === 'el')
			$data[]=trim($l['label']);
	}
}

file_put_contents('/opt/gretaste/gretaste.dict', implode(PHP_EOL, $data));
?>
