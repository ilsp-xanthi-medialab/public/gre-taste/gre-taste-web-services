#!/bin/bash

mv morphology.json morphology.json.old
wget http://gretaste.ilsp.gr/morphology.json

mv dictionary.json dictionary.json.old
wget http://gretaste.ilsp.gr/dictionary.json
mv dictionary.general.json dictionary.general.json.old
wget http://gretaste.ilsp.gr/dictionary.general.json

php create.spell.php
