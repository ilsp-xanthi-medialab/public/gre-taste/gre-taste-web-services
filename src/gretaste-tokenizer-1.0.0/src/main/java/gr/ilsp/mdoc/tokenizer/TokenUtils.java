package gr.ilsp.mdoc.tokenizer;

/**
 *
 * @author pminos
 */
public class TokenUtils {

    public static String content(int type, String content) {
        if (type == TokenizerImpl.WORD || type == TokenizerImpl.SYMBOL
                || type == TokenizerImpl.OTHER || type == TokenizerImpl.LATIN) {
            return content;
        }

        return "$" + TokenizerImpl.typeName(type) + "$";
    }
}
