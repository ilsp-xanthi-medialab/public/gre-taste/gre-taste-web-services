package gr.ilsp.mdoc.tokenizer;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.text.Normalizer;
import java.util.HashSet;
import java.util.Set;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author pminos
 */
public class Demo {

    /**
     * @param args the command line arguments
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws IOException {
        if (args.length != 2) {
            System.err.println("Usage: tokenize input.txt output.html");
//            args = new String[]{"export_gt.txt", "export_gt.txt.html"};
//            args = new String[]{"TLG2702-001.txt", "TLG2702-001.txt.html"};
            System.exit(1);
        }
        String content = FileUtils.readFileToString(new File(args[0]));
        content = Normalizer.normalize(content, Normalizer.Form.NFC);
        TokenizerImpl impl = new TokenizerImpl(new StringReader(content));
        StringBuilder p = new StringBuilder();
        p.append("<!DOCTYPE html>\n<html>\n<head>\n\t<meta charset=\"utf-8\">\n\t<title></title>\n</head>\n<body>\n\t<p>");

        Set<String> words = new HashSet<>();
        Set<String> numeral = new HashSet<>();
        Set<String> symbol = new HashSet<>();
        Set<String> other = new HashSet<>();
        Set<String> latin = new HashSet<>();
        int[] counts = new int[6];
        while (true) {
            int type = impl.getNextToken();

            if (type == TokenizerImpl.EOF) {
                break;
            }

            counts[type]++;
            String color = "#ffffff";
            switch (type) {
                case TokenizerImpl.WHITESPACE:
                    color = "#ffff80";
                    break;

                case TokenizerImpl.WORD:
                    words.add(impl.yytext());
                    break;

                case TokenizerImpl.NUMERAL:
                    numeral.add(impl.yytext());
                    color = "#00ffff";
                    break;

                case TokenizerImpl.SYMBOL:
                    color = "#ff99ff";
                    symbol.add(impl.yytext());
                    break;

                case TokenizerImpl.OTHER:
                    other.add(impl.yytext());
                    color = "#ff0000";
                    break;

                case TokenizerImpl.LATIN:
                    latin.add(impl.yytext());
                    color = "#d0d0d0";
                    break;

                default:
            }
            p.append("<span style=\"background-color: ").append(color).append("\">");
            p.append(impl.yytext());
            p.append("</span>");
//            if(tp.equals("other"))
//                System.out.println(impl.yytext());
//            p.append("<span style=\"display:none\">");
//            p.append(tp);
//            p.append("</span>");
            if (impl.yytext().equals("\n")) {
                p.append("<br>");
            }
        }
        p.append("<p>").append(String.format("Space: %d, Word: %d, Numeral: %d, Symbol: %d, Other: %d, Latin: %d", counts[0], counts[1], counts[2], counts[3], counts[4], counts[5])).append("</p>");
        p.append("<h1>Words / ");
        p.append(words.size());
        p.append("</h1>");
        p.append("<p>");
        for (String w : words) {
            p.append(w).append("<br>");
        }
        p.append("</p>");
        if (!numeral.isEmpty()) {
            p.append("<h1>Numerals / ");
            p.append(numeral.size());
            p.append("</h1>");
            p.append("<p>");
            for (String w : numeral) {
                p.append(w).append("<br>");
            }
            p.append("</p>");
        }
        if (!other.isEmpty()) {
            p.append("<h1>Other / ");
            p.append(other.size());
            p.append("</h1>");
            p.append("<p>");
            for (String w : other) {
                p.append(w).append("<br>");
            }
            p.append("</p>");
        }
        if (!symbol.isEmpty()) {
            p.append("<h1>Symbols / ");
            p.append(symbol.size());
            p.append("</h1>");
            p.append("<p>");
            for (String w : symbol) {
                p.append(w).append("<br>");
            }
            p.append("</p>");
        }
        if (!latin.isEmpty()) {
            p.append("<h1>Latin / ");
            p.append(latin.size());
            p.append("</h1>");

            p.append("<p>");
            for (String w : latin) {
                p.append(w).append("<br>");
            }
            p.append("</p>");
        }
        p.append("</p>\n</body>\n</html>");
        FileUtils.write(new File(args[1]), p.toString(), "UTF-8");
    }

}
