package gr.ilsp.mdoc.speller.common;

import java.util.Arrays;
import java.util.Set;
import java.util.TreeSet;

/**
 *
 * @author pminos
 */
public class Optical1 {

    static char[] characters;
    static int[] classes;
    static String[] optical = {
        "ὰάᾰᾱᾲᾳᾴᾶᾷἀἁἂἃἄἅἆἇᾀᾁᾂᾃᾄᾅᾆᾇάα",
        "ὠὡὢὣὤὥὦὧῲῳῴῶῷᾠᾡᾢᾣᾤᾥᾦᾧωώὼώϖ",
        "ὴήήηᾐᾑᾒᾓᾔᾕᾖᾗῂῃῄῆῇἠἡἢἣἤἥἦἧ",
        "ΆΑᾸᾹᾺΆᾼᾈᾉᾊᾋᾌᾍᾎᾏἉἊἋἌἍἎἏἈ",
        "ΏΩὨὩὪὫὬὭὮὯῺΏῼᾨᾩᾪᾫᾬᾭᾮᾯ",
        "ΉΗἨἩἪἫἬἭἮἯᾘᾙᾚᾛᾜᾝᾞᾟῊΉῌ",
        "ὐὑὒὓὔὕὖὗῠῡῢΰῦῧΰυύϋὺύ",
        "ΊΙΪἸἹἺἻἼἽἾἿῘῙῚΊ",
        "ΐἰἱἲἳἴἵἶἷὶίίιϊῐῑῒΐῖῗ",
        "ΎΥΫὙὛὝὟῨῩῪΎϒϓϔ",
        "ΌΟῸΌὈὉὊὋὌὍ",
        "ΈΕἘἙἚἛἜἝῈΈ",
        "ὀὁὂὃὄὅοόὸό",
        "ὲέέεἐἑἒἓἔἕ",
        "ῤῥρ",
        "ῬΡ",};

    static {
        Set<Character> tmp = new TreeSet<>();
        for (String o : optical) {
            for (char c : o.toCharArray()) {
                tmp.add(c);
            }
        }
        StringBuffer b = new StringBuffer();
        for (Character c : tmp) {
            b.append(c);
        }
        characters = b.toString().toCharArray();
        classes = new int[characters.length];
        for (int i = 0; i < characters.length; i++) {
            char c = characters[i];
            for (int j = 0; j < optical.length; j++) {
                if (optical[j].indexOf(c) >= 0) {
                    classes[i] = j;
                    break;
                }

            }
        }

    }

    public static boolean sameClass(char a, char b) {
        int v1 = Arrays.binarySearch(characters, a);
        if (v1 < 0) {
            return false;
        }
        int v2 = Arrays.binarySearch(characters, b);
        if (v2 < 0) {
            return false;
        }

        return classes[v1] == classes[v2];

    }
}
