package gr.ilsp.mdoc.speller;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author pminos
 */
class Line {

    int offset;
    String content;
    List<Token> tokens = new ArrayList<>();
    List<Token> wtokens = new ArrayList<>();

    @Override
    public String toString() {
        return "Line{" + "tokens=" + tokens + '}';
    }

    void addToken(Token token) {
        tokens.add(token);
        if (token.type != 0) {
            wtokens.add(token);
        }
    }

}
