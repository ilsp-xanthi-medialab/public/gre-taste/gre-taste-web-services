package gr.ilsp.mdoc.speller.dawg;

import java.util.Arrays;

/**
 *
 * @author pminos
 */
@SuppressWarnings("ClassWithoutLogger")
public class DawgState {

    private char[] symbols = new char[0];
    private DawgState[] states = new DawgState[0];
    private boolean accept = false;
//    public int[] ids = new int[0];
//
//    public void addId(int id) {
//        int[] ids = new int[this.ids.length + 1];
//        for (int i = 0; i < this.ids.length; i++) {
//            ids[i] = this.ids[i];
//        }
//        ids[this.ids.length] = id;
//        this.ids = ids;
//    }

    @SuppressWarnings("ReturnOfCollectionOrArrayField")
    public char[] getSymbols() {
        return symbols;
    }

    @SuppressWarnings("ReturnOfCollectionOrArrayField")
    public DawgState[] getStates() {
        return states;
    }

    public DawgState getTransition(char symbol) {
        int idx = Arrays.binarySearch(symbols, symbol);
        if (idx >= 0) {
            return states[idx];
        }
        return null;
    }

    public DawgState addTransition(char symbol, DawgState state) {
        int idx = Arrays.binarySearch(symbols, symbol);
        if (idx >= 0) {
            DawgState old = states[idx];
            states[idx] = state;
            return old;
        }
        symbols = Arrays.copyOf(symbols, symbols.length + 1);
        states = Arrays.copyOf(states, states.length + 1);
        idx++;
        idx = -idx;
        for (int i = symbols.length - 1; i > idx; i--) {
            symbols[i] = symbols[i - 1];
            states[i] = states[i - 1];
        }
        symbols[idx] = symbol;
        states[idx] = state;
        return state;
    }

    public boolean isAccept() {
        return accept;
    }

    public void setAccept(boolean accept) {
        this.accept = accept;
    }

    boolean hasChildren() {
        return symbols.length != 0;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + Arrays.hashCode(this.symbols);
        hash = 97 * hash + Arrays.hashCode(this.states);
        hash = 97 * hash + (this.accept ? 1 : 0);
        return hash;
    }

    @Override
    @SuppressWarnings("AccessingNonPublicFieldOfAnotherObject")
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DawgState other = (DawgState) obj;
        if (this.accept != other.accept) {
            return false;
        }
        if (!Arrays.equals(this.symbols, other.symbols)) {
            return false;
        }
        if (!Arrays.equals(this.states, other.states)) {
            return false;
        }
        return true;
    }

}
