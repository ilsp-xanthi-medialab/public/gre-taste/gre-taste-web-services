/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.ilsp.mdoc.speller.dawg;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author pminos
 */
public class AutomatonMatcher {

    private final Dawg automaton;
    private final CharSequence chars;

    AutomatonMatcher(Dawg dawg, CharSequence chars) {
        this.automaton = dawg;
        this.chars = chars;
    }

    private int matchStart = -1;
    private int matchEnd = -1;
    private DawgState matchState = null;

    /**
     * Find the next matching subsequence of the input.
     * <br>
     * This also updates the values for the {@code start}, {@code end}, and
     * {@code group} methods.
     *
     * @return {@code true} if there is a matching subsequence.
     */
    public boolean find() {
        int begin;
        switch (getMatchStart()) {
            case -2:
                return false;
            case -1:
                begin = 0;
                break;
            default:
                begin = getMatchEnd();
                // This occurs when a previous find() call matched the empty string. This can happen when the pattern is a* for example.
                if (begin == getMatchStart()) {
                    begin += 1;
                    if (begin > getChars().length()) {
                        setMatch(-2, -2);
                        return false;
                    }
                }
        }

        int match_start;
        int match_end;

        match_start = -1;
        match_end = -1;
        DawgState match_state = null;
        int l = getChars().length();
        while (begin < l) {
            DawgState p = automaton.getInitial();
            for (int i = begin; i < l; i++) {
                final DawgState new_state = automaton.step(p, getChars().charAt(i));
                if (new_state == null) {
                    break;
                } else if (automaton.isAccept(new_state)) {
                    // found a match from begin to (i+1)
                    match_start = begin;
                    match_end = (i + 1);
                    match_state = new_state;
                }
                p = new_state;
            }
            if (match_start != -1) {
                setMatch(match_start, match_end);
                matchState = match_state;
                return true;
            }
            begin += 1;
        }
        if (match_start != -1) {
            setMatch(match_start, match_end);
            matchState = match_state;
            return true;
        } else {
            setMatch(-2, -2);
            matchState = null;
            return false;
        }
    }

    private void setMatch(final int matchStart, final int matchEnd) throws IllegalArgumentException {
        if (matchStart > matchEnd) {
            throw new IllegalArgumentException("Start must be less than or equal to end: " + matchStart + ", " + matchEnd);
        }
        this.matchStart = matchStart;
        this.matchEnd = matchEnd;
    }

    private int getMatchStart() {
        return matchStart;
    }

    private int getMatchEnd() {
        return matchEnd;
    }

    private CharSequence getChars() {
        return chars;
    }

    /**
     * Returns the offset after the last character matched.
     *
     * @return The offset after the last character matched.
     * @throws IllegalStateException if there has not been a match attempt or if
     * the last attempt yielded no results.
     */
    public int end() throws IllegalStateException {
        matchGood();
        return matchEnd;
    }

    /**
     * Returns the subsequence of the input found by the previous match.
     *
     * @return The subsequence of the input found by the previous match.
     * @throws IllegalStateException if there has not been a match attempt or if
     * the last attempt yielded no results.
     */
    public String group() throws IllegalStateException {
        matchGood();
        return chars.subSequence(matchStart, matchEnd).toString();
    }

    /**
     * Returns the offset of the first character matched.
     *
     * @return The offset of the first character matched.
     * @throws IllegalStateException if there has not been a match attempt or if
     * the last attempt yielded no results.
     */
    public int start() throws IllegalStateException {
        matchGood();
        return matchStart;
    }

    /**
     * Helper method to check that the last match attempt was valid.
     */
    private void matchGood() throws IllegalStateException {
        if ((matchStart < 0) || (matchEnd < 0)) {
            throw new IllegalStateException("There was no available match.");
        }
    }

    public List<String> ids() {

        DawgState last = matchState.getTransition('\0');
        List<String> ids = new ArrayList<>();
        this.automaton.collectSuffixes(last, ids, new StringBuilder(), 0);
        return ids;

    }

}
