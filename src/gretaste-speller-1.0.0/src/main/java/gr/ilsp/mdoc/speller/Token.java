package gr.ilsp.mdoc.speller;

import gr.ilsp.mdoc.tokenizer.TokenUtils;

/**
 *
 * @author pminos
 */
class Token {

    final String content;
    final int type;
    final int offset;
    boolean correct = true;
    boolean splitted;
    boolean second = false;
    String full;
    String correction;

    Token(String content, int type, int offset) {
        this.content = content;
        this.type = type;
        this.offset = offset;
    }

    @Override
    public String toString() {
        return "Token{" + "content=" + content + ", type=" + type + '}';
    }

    String content() {
        if (splitted) {
            return TokenUtils.content(type, full);
        }
        return TokenUtils.content(type, content);
    }

}
