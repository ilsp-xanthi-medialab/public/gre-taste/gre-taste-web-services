package gr.ilsp.mdoc.speller;

import gr.ilsp.mdoc.speller.common.Suggestion;
import gr.ilsp.mdoc.tokenizer.TokenizerImpl;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author pminos
 */
public class MdocSpeller {

    private final Speller speller = new Speller();

    public MdocSpeller(String file) {
        try {
            List<String> lines = FileUtils.readLines(new File(file), "UTF-8");
            speller.load(lines);
        } catch (IOException ex) {
            Logger.getLogger(MdocSpeller.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String autocorrect(String content) {
        return autocorrect(content, null);
    }

    public String autocorrect(String content, List<String[]> tokens) {

        List<Line> list = process(content);
        StringBuilder b = new StringBuilder();
        for (Line line : list) {
            for (Token span : line.tokens) {
                if (span.correction == null) {
                    b.append(span.content);

                    if (tokens != null && !span.content.trim().isEmpty()) {
                        tokens.add(new String[]{span.content, span.content, "OK"});
                    }
                } else {
                    b.append(span.correction);
                    if (tokens != null && !span.content.trim().isEmpty()) {
                        tokens.add(new String[]{span.content, span.correction, "ERR"});
                    }
                }
            }
            b.append("\n");
        }
        return b.toString();
    }

    private List<Line> process(String content) {
        content = Normalizer.normalize(content, Normalizer.Form.NFC);
        content = content.replace('µ', 'μ');
        List<Line> lines = segment(content);
        markCorrect(lines);

        for (Line l : lines) {
            for (Token t : l.tokens) {

                if (t.type == TokenizerImpl.WORD) {

                    if (!t.correct) {
                        //String[] prev = findPrev(lines, l, t);
                        //t.correction = getPossible(prev[0], prev[1], t.content);
                        t.correction = getPossible(t.content);
                        //System.out.println(t.content+"\t->\t"+t.correction);
                    }
                }
            }
        }
        return lines;
    }

    private List<Line> segment(String content) {
        List<Line> list = new ArrayList<>();

        String[] lines = content.split("\n");
        int offset = 0;
        for (String l : lines) {
            Line line = new Line();
            line.offset = offset;
            line.content = l;
            list.add(line);
            offset += l.length();
            offset++;
        }
        TokenizerImpl impl;
        for (Line line : list) {
            try {
                impl = new TokenizerImpl(new StringReader(line.content));
                while (true) {
                    int type = impl.getNextToken();
                    if (type == TokenizerImpl.EOF) {
                        break;
                    }
                    Token token = new Token(impl.yytext(), type, impl.yychar());

                    line.addToken(token);
                }
            } catch (IOException ex) {
                Logger.getLogger(MdocSpeller.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return list;

    }

    private void markCorrect(List<Line> list) {
        for (Line l : list) {
            for (Token t : l.tokens) {
                if (t.type == TokenizerImpl.WORD) {
                    if (!speller.spell(t.content)) {
                        t.correct = false;
                    }
                }
            }
        }
        try {
            for (int i = 0; i < list.size() - 1; i++) {
                Token last = list.get(i).tokens.get(list.get(i).tokens.size() - 1);
                Token next = list.get(i + 1).tokens.get(0);
                if (last.type == 1 && next.type == 1) {
                    if (last.content.endsWith("-")) {
                        last.splitted = true;
                        next.splitted = true;
                        next.second = true;

                        String word = last.content.substring(0, last.content.length() - 1);
                        word += next.content;
                        last.full = word;
                        next.full = word;

                        if (speller.spell(word)) {
                            last.correct = true;
                            next.correct = true;
                        } else {
                            last.correct = false;
                            next.correct = false;
                        }
                    }
                }
            }
        } catch (Throwable t) {
        }
        for (Line l : list) {
            for (int i = 1; i < l.wtokens.size(); i++) {
                Token t0, t1;
                t0 = l.wtokens.get(i - 1);
                t1 = l.wtokens.get(i);
                if (!t0.correct && !t1.correct) {
                    if (speller.spell(t0.content + t1.content)) {
                        //System.out.println(t0.content + t1.content);
                        t0.correct = true;
                        t1.correct = true;
                        t0.correction = t0.content + t1.content;
                        t1.correction = "";
//                        System.out.println(t0.content);
                    }
                }
            }
        }
    }

    private String[] findPrev(List<Line> list, Line line, Token t) {
        List<Token> wtokens = new ArrayList<>();
        for (Line ll : list) {
            wtokens.addAll(ll.wtokens);
        }
        String[] f;
        int idx = wtokens.indexOf(t);
        int prev = idx - 1;
        if (prev >= 0) {
            if (wtokens.get(idx).second && wtokens.get(idx).splitted) {
                prev--;
            }
        }
        int pprev = prev - 1;
        if (pprev >= 0) {
            if (wtokens.get(prev).second && wtokens.get(prev).splitted) {
                pprev--;
            }
        }

        if (pprev >= 0) {
            f = new String[]{wtokens.get(pprev).content(), wtokens.get(prev).content()};
        } else {
            if (prev >= 0) {
                f = new String[]{"", wtokens.get(prev).content()};
            } else {
                f = new String[]{"", ""};
            }
        }
        return f;
    }

    @SuppressWarnings("Convert2Lambda")
    private String getPossible(String w) {

        TreeSet<Suggestion> suggestions = speller.suggest(w, 1);
        if (suggestions.isEmpty()) {
            suggestions = speller.suggest(w, 2);
        }
        if (suggestions.isEmpty()) {
            suggestions = speller.suggest(w, 3);
        }

        if (suggestions.isEmpty()) {
            return w;
        }

        return suggestions.first().getWord();
    }

}
