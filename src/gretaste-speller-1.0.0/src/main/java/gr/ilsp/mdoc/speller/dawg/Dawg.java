package gr.ilsp.mdoc.speller.dawg;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author pminos
 */
@SuppressWarnings("ClassWithoutLogger")
public class Dawg {

    private final DawgState initial;

    public Dawg(DawgState initial) {
        this.initial = initial;
    }

    public DawgState getInitial() {
        return initial;
    }

    public boolean accepts(CharSequence sequence) {
        DawgState state = initial;
        int length = sequence.length();
        for (int i = 0; i < length; i++) {
            DawgState next = state.getTransition(sequence.charAt(i));
            if (next == null) {
                return false;
            }
            state = next;
        }
        return state.isAccept();
    }

//    public boolean setId(String sequence, int id) {
//        DawgState state = initial;
//        int length = sequence.length();
//        for (int i = 0; i < length; i++) {
//            DawgState next = state.getTransition(sequence.charAt(i));
//            if (next == null) {
//                return false;
//            }
//            state = next;
//        }
//        if(state.isAccept()) {
//            state.addId(id);
//        }
//        return state.isAccept();
//    }
//
//    public int[] getId(String sequence) {
//
//        DawgState state = initial;
//        int length = sequence.length();
//        for (int i = 0; i < length; i++) {
//            DawgState next = state.getTransition(sequence.charAt(i));
//            if (next == null) {
//                return new int[0];
//            }
//            state = next;
//        }
//        if(state.isAccept()) {
//            return state.ids;
//        }else
//        return new int[0];
//    }
    public List<String> lookup(String sequence) {
        DawgState state = initial;
        int length = sequence.length();
        for (int i = 0; i < length; i++) {
            DawgState next = state.getTransition(sequence.charAt(i));
            if (next == null) {
                return new ArrayList<>();
            }
            state = next;
        }
        DawgState last = state.getTransition('\0');
        List<String> ids = new ArrayList<>();
        collectSuffixes(last, ids, new StringBuilder(), 0);
        return ids;
    }

     void collectSuffixes(DawgState state, List<String> ids, StringBuilder sb, int idx) {
        if (state.isAccept()) {
            ids.add(sb.toString());
        }
        for (int i = 0; i < state.getSymbols().length; i++) {
            sb.append(state.getSymbols()[i]);
            collectSuffixes(state.getStates()[i], ids, sb, 0);
            sb.setLength(sb.length() - 1);
        }
    }

    public AutomatonMatcher newMatcher(String s) {
        return new AutomatonMatcher(this, s);
    }

    boolean isAccept(DawgState state) {
        return state.getTransition('\0') != null;
    }

    DawgState step(DawgState state, char c) {
        return state.getTransition(c);
    }

}
