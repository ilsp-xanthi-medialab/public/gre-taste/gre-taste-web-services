package gr.ilsp.mdoc.speller.common;

import java.util.List;

/**
 *
 * @author pminos
 */
public class Suggestion implements Comparable<Suggestion> {

    private final String word;
    private final double distance;
    private int freq;
    private List<String> how;

    public Suggestion(String word, int distance) {
        this.word = word;
        this.distance = distance;
    }

    public Suggestion(String word, double distance) {
        this.word = word;
        this.distance = distance;
    }

    public Suggestion(CharSequence word, int distance) {
        this.word = word.toString();
        this.distance = distance;
    }

    public Suggestion(String word, int distance, List<String> how) {
        this.word = word;
        this.distance = distance;
        this.how = how;
    }

    public int getFreq() {
        return freq;
    }

    public void setFreq(int freq) {
        this.freq = freq;
    }

    public String getWord() {
        return word;
    }

    public double getDistance() {
        return distance;
    }

    @Override
    public int compareTo(Suggestion o) {
        if (word.compareTo(o.word) == 0) {
            if (distance == o.distance) {
                return 0;
            }
            return distance > o.distance ? 1 : -1;
        }
        return word.compareTo(o.word);
    }

    @Override
    public String toString() {
        return word + "/" + distance;
    }

    public String full() {
        String h = "\n";
        for (String hh : how) {
            h += "\t" + hh + "\n";
        }
        return word + "/" + distance + "\t" + h;
    }
}
