package gr.ilsp.mdoc.speller.oflazer;

import gr.ilsp.mdoc.speller.common.Suggestion;
import gr.ilsp.mdoc.speller.common.Optical;
import gr.ilsp.mdoc.speller.common.Optical1;
import gr.ilsp.mdoc.speller.dawg.Dawg;
import gr.ilsp.mdoc.speller.dawg.DawgState;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeSet;

/**
 *
 * @author pminos
 */
@SuppressWarnings("ClassWithoutLogger")
public class OflazerSpeller {

    private final Dawg fsa;

    Map<Character, Integer> classes = new HashMap<>();

    public OflazerSpeller(final Dawg dictionary) {
        this.fsa = dictionary;
        for (int i = 0; i < Optical.optical.length; i++) {
            for (char c : Optical.optical[i].toCharArray()) {
                classes.put(c, i);
            }
        }
    }

    public TreeSet<Suggestion> suggest(String word, int distance) {
        SuggestState state = new SuggestState(word, distance);

        findRepl(state, 0, fsa.getInitial());

        Collections.sort(state.candidates);
        final TreeSet<Suggestion> candStringList = new TreeSet<Suggestion>();
        for (final Suggestion cd : state.candidates) {
            candStringList.add(new Suggestion(cd.getWord(), cd.getDistance()));
        }
        return candStringList;
    }

    private void addCandidate(SuggestState state, final int depth, final int dist) {
        final StringBuilder sb = new StringBuilder(depth);
        sb.append(state.candidate, 0, depth + 1);
        state.candidates.add(new Suggestion(sb.toString(), dist));
    }

    private void findRepl(SuggestState state, final int depth, final DawgState node) {
        if (depth == state.candidate.length) {
            return;
        }
        int size = node.getSymbols().length;

        for (int i = 0; i < size; i++) {
            state.candidate[depth] = node.getSymbols()[i];

            if (cuted(state, depth) <= state.editDistance) {
                findRepl(state, depth + 1, node.getStates()[i]);
            }
            if (node.getStates()[i].isAccept()) {
                int dist = ed(state, state.wordLen - 1, depth);
                if (dist <= state.editDistance) {
                    addCandidate(state, depth, dist);
                }
            }
        }
    }

    private int cuted(SuggestState state, final int depth) {
        //-1 for epsilon, 0 for no epsilon
        final int l = Math.max(-1, depth - state.editDistance); // min chars from word to consider - 1
        final int u = Math.min(state.wordLen - 1, depth + state.editDistance); // max chars from word to

        int min_ed = state.editDistance + 1; // what is to be computed
        int d;
//        System.out.println("cuted: " + depth);
        for (int i = l; i <= u; i++) {
            d = ed(state, i, depth);
            if (d < min_ed) {
                min_ed = d;
            }
        }
        return min_ed;
    }

    public static void main(String[] args) {
    }

    public static int ed(SuggestState state, final int m, final int n) {

        int result;
        int a, b, c; // not really necessary
        if (m == -1 || n == -1) {
            //for e
            result = Math.max(m + 1, n + 1);
        } else if (state.word_ff[m] == state.candidate[n]) {
            // last characters are the same
            result = state.get(m, n);
        } else if (m > 0 && n > 0 && state.word_ff[m] == state.candidate[n - 1] && state.word_ff[m - 1] == state.candidate[n]) {
            // last two characters are transposed
            a = state.get(m - 1, n - 1); // transposition, e.g. ababab, ababba
            b = state.get(m + 1, n); // deletion, e.g. abab, aba
            c = state.get(m, n + 1); // insertion e.g. aba, abab
            result = 1 + min(a, b, c);
        } else {
            // otherwise
            a = state.get(m, n); // replacement, e.g. ababa, ababb
            b = state.get(m + 1, n); // deletion, e.g. ab, a
            c = state.get(m, n + 1); // insertion e.g. a, ab
            result = 1 + min(a, b, c);

            if (Optical1.sameClass(state.word_ff[m], state.candidate[n])) {
                result = 0 + min(a, b, c);
            }
//            if (state.word_ff[m] == 'Δ' && state.candidate[n] == '∆') {
//                result = 0 + min(a, b, c);
//            }
//            if (state.word_ff[m] == '∆' && state.candidate[n] == 'Δ') {
//                result = 0 + min(a, b, c);
//            }
        }

        state.set(m + 1, n + 1, result);

//        System.err.format("ED\t%s@m=%d\t%s@n=%d = %d\n", new String(state.word_ff, 0, m + 1), m, new String(state.candidate, 0, n + 1), n, result);
//        System.err.println(state.toString(new String(state.word_ff, 0, m + 1), new String(state.candidate, 0, n + 1)));
        return result;
    }

    private static int min(final int a, final int b, final int c) {
        return Math.min(a, Math.min(b, c));
    }

    private boolean sameClass(char c1, char c2) {
        Integer cl1 = classes.get(c1);
        Integer cl2 = classes.get(c2);
        if (cl1 != null && cl2 != null) {
            return cl1.equals(cl2);
        }
//        for(String c:Optical.optical) {
//            if(c.indexOf(c1)!= -1)
//                if(c.indexOf(c2)!= -1)
//                    return true;
//        }
        return false;
    }

}
