package gr.ilsp.mdoc.speller.dawg;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Incremental Construction of Minimal Acyclic Finite-State Automata, Jan
 * Daciuk; Bruce W. Watson; Stoyan Mihov; Richard E. Watson
 *
 * @author pminos
 */
@SuppressWarnings("ClassWithoutLogger")
public class DaciukMihovAutomatonBuilder {

    private final DawgState initial = new DawgState();
    @SuppressWarnings("Convert2Diamond")
    private Map<DawgState, DawgState> register = new HashMap<DawgState, DawgState>();

    private void add(CharSequence sequence) {
        DawgState state = initial;
        int length = sequence.length();
        int i;
        for (i = 0; i < length; i++) {
            DawgState next = state.getTransition(sequence.charAt(i));
            if (next == null) {
                break;
            }
            state = next;
        }
        if (i == length) {
            state.setAccept(true);
        } else {
            if (state.hasChildren()) {
                replaceOrRegister(state);
            }
            addSuffix(state, sequence, i);
        }
    }

    private void replaceOrRegister(DawgState state) {
        final DawgState child = state.getStates()[state.getStates().length - 1];

        if (child.hasChildren()) {
            replaceOrRegister(child);
        }

        DawgState registered = register.get(child);
//        registered=null;
        if (registered != null) {
            state.getStates()[state.getStates().length - 1] = registered;
        } else {
            register.put(child, child);
        }
    }

    @SuppressWarnings("AssignmentToMethodParameter")
    private void addSuffix(DawgState state, CharSequence sequence, int pos) {
        final int len = sequence.length();
        for (int i = pos; i < len; i++) {
            char symbol = sequence.charAt(i);
            DawgState next = new DawgState();
            state.addTransition(symbol, next);
            state = next;
        }
        state.setAccept(true);
    }

    private void complete() {
        if (this.register == null) {
            throw new IllegalStateException();
        }
        if (initial.hasChildren()) {
            replaceOrRegister(initial);
        }
        register = null;
    }

    public DawgState create(String[] words) {
        Arrays.sort(words);
        for (String w : words) {
            add(w);
        }
        complete();
        return initial;
    }

}
