package gr.ilsp.mdoc.speller.common;

import java.util.Arrays;

/**
 *
 * @author pminos
 */
public class Optical {

    static char[][] classes;
    public static String[] optical = {
        "ὰάᾰᾱᾲᾳᾴᾶᾷἀἁἂἃἄἅἆἇᾀᾁᾂᾃᾄᾅᾆᾇάα",
        "ὠὡὢὣὤὥὦὧῲῳῴῶῷᾠᾡᾢᾣᾤᾥᾦᾧωώὼώϖ",
        "ὴήήηᾐᾑᾒᾓᾔᾕᾖᾗῂῃῄῆῇἠἡἢἣἤἥἦἧ",
        "ΆΑᾸᾹᾺΆᾼᾈᾉᾊᾋᾌᾍᾎᾏἉἊἋἌἍἎἏἈ",
        "ΏΩὨὩὪὫὬὭὮὯῺΏῼᾨᾩᾪᾫᾬᾭᾮᾯ",
        "ΉΗἨἩἪἫἬἭἮἯᾘᾙᾚᾛᾜᾝᾞᾟῊΉῌ",
        "ὐὑὒὓὔὕὖὗῠῡῢΰῦῧΰυύϋὺύ",
        "ΊΙΪἸἹἺἻἼἽἾἿῘῙῚΊ",
        "ΐἰἱἲἳἴἵἶἷὶίίιϊῐῑῒΐῖῗ",
        "ΎΥΫὙὛὝὟῨῩῪΎϒϓϔ",
        "ΌΟῸΌὈὉὊὋὌὍ",
        "ΈΕἘἙἚἛἜἝῈΈ",
        "ὀὁὂὃὄὅοόὸό",
        "ὲέέεἐἑἒἓἔἕ",
        "ῤῥρ",
        "ῬΡ",};

    static {
        classes = new char[optical.length][];
        for (int i = 0; i < optical.length; i++) {
            classes[i] = optical[i].toCharArray();
            Arrays.sort(classes[i]);
        }
    }

    static boolean same(char a, char b) {
        if (a == b) {
            return true;
        }
        for (char[] cl : classes) {
            int v1 = Arrays.binarySearch(cl, a);
            int v2 = Arrays.binarySearch(cl, b);
            if (v1 < 0 && v2 < 0) {
                continue;
            }
            return true;
        }
        return false;
    }
}
