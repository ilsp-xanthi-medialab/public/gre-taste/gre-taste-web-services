package gr.ilsp.mdoc.speller.oflazer;

import gr.ilsp.mdoc.speller.common.Suggestion;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author pminos
 */
class SuggestState {

    final int editDistance;
    char[] candidate;
    final int wordLen;
    final char[] word_ff;
    final List<Suggestion> candidates = new ArrayList<Suggestion>();
    private int[][] matrix;

    int get(int i, int j) {
        if (i >= j + 1 + editDistance) {
            return editDistance + 1;
        }
        if (j >= i + 1 + editDistance) {
            return editDistance + 1;
        }
        return matrix[i][j];
    }

    SuggestState(String word, int distance) {
        this.editDistance = distance;
        candidate = new char[word.length() + distance];
        word_ff = word.toCharArray();
        wordLen = word_ff.length;
        matrix = new int[word.length() + 1][word.length() + distance + 1];
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                //matrix[i][j] = Math.abs(i - j);
            }
            //matrix[i][0] = i;
        }
        for (int i = 0; i < matrix.length; i++) {
            matrix[i][0] = i;
        }
        for (int i = 0; i < matrix[0].length; i++) {
            matrix[0][i] = i;
        }
    }

    String toString(String w, String c) {
        String x = "-----------------\n";
        x += "     ";
        for (int i = 0; i < matrix.length; i++) {
            if (i < c.length()) {
                x += c.charAt(i) + "  ";
            } else {
                x += "?  ";
            }
        }
        x += "\n";
        for (int i = 0; i < matrix.length; i++) {
            ArrayList<String> l = new ArrayList();

            if (i == 0) {
                x += " ";
            } else if (i - 1 < w.length()) {
                x += w.charAt(i - 1) + "";
            } else {
                x += "?";
            }

            for (int j = 0; j < matrix[i].length; j++) {
                try {
                    l.add(matrix[i][j] + "");
                } catch (Throwable t) {
                    l.add(" ");
                }
            }
            x += l + "\n";
        }
        return x;
    }

    void set(int i, int j, int v) {
        matrix[i][j] = v;
    }
}
