package gr.ilsp.mdoc.speller;

import gr.ilsp.mdoc.speller.common.Suggestion;
import gr.ilsp.mdoc.speller.dawg.DaciukMihovAutomatonBuilder;
import gr.ilsp.mdoc.speller.dawg.Dawg;
import gr.ilsp.mdoc.speller.dawg.DawgState;
import gr.ilsp.mdoc.speller.oflazer.OflazerSpeller;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.text.Normalizer;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.TreeSet;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

/**
 *
 * @author pminos
 */
public class Speller {

    OflazerSpeller speller;
    Dawg dawg;

    void load(String line) throws IOException {
        String[] a = new String[1];
        a[0] = line;
        for (int i = 0; i < a.length; i++) {
            a[i] = Normalizer.normalize(a[i], Normalizer.Form.NFC);
        }
        DaciukMihovAutomatonBuilder b = new DaciukMihovAutomatonBuilder();
        DawgState state = b.create(a);
        dawg = new Dawg(state);
        speller = new OflazerSpeller(dawg);
    }

    void load(Collection<String> lines) {
        String[] a = lines.toArray(new String[0]);
        for (int i = 0; i < a.length; i++) {
            //a[i] = Normalizer.normalize(a[i], Normalizer.Form.NFC);
        }
        DaciukMihovAutomatonBuilder b = new DaciukMihovAutomatonBuilder();
        DawgState state = b.create(a);
        dawg = new Dawg(state);
        speller = new OflazerSpeller(dawg);
    }

    public Speller(String file) throws IOException {

        List<String> lines = FileUtils.readLines(new File(file), "UTF-8");
        this.load(lines);
    }

    public Speller() {
    }

    public Speller(InputStream inputStream) throws IOException {
        StringWriter writer = new StringWriter();
        IOUtils.copy(inputStream, writer, "UTF-8");
        String theString = writer.toString();
        String[] lines = theString.split("\n");
        load(Arrays.asList(lines));
    }

    public boolean spell(String word) {
        word = Normalizer.normalize(word, Normalizer.Form.NFC);
        if (dawg.accepts(word)) {
            return true;
        }

        char f = word.charAt(0);
        if (Character.isUpperCase(f)) {
            String xword = Character.toLowerCase(f) + word.substring(1);
            if (dawg.accepts(xword)) {
                return true;
            }
        }
        return false;
    }

    public TreeSet<Suggestion> suggest(String word, int distance) {
        TreeSet<Suggestion> result = speller.suggest(word, distance);
        return result;
    }
}
