/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.ilsp.gretaste.mt;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author pminos
 */
public class TranslatedToken {

    public int id;
    public int position;
    public String wf;
    public String lemma;

    public  boolean translated = false;
    public String translation;
    public int lexicon = 0;
    public boolean startOfMW = false;
    public boolean partOfMW = false;
    public int mwSize;
    public List<Integer> entity_ids = new ArrayList<>();
    public TranslatedToken _partOfMW;
}
