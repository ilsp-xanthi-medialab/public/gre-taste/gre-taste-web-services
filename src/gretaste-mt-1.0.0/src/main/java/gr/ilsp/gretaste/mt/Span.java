/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.ilsp.gretaste.mt;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author pminos
 */
public class Span {

    int start;
    int end;
    List<String> ids = new ArrayList<>();

    public Span(int start, int end) {
        this.start = start;
        this.end = end;
    }

    public Span(int start, int end, List<String> ids) {
        this.start = start;
        this.end = end;
        if (null != ids) {
            this.ids.addAll(ids);
        }
    }

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public int getEnd() {
        return end;
    }

    public void setEnd(int end) {
        this.end = end;
    }

    public boolean contains(int start, int end) {
        if (end < this.start) {
            return false;
        }
        if (start > this.end) {
            return false;
        }
        return true;
    }

    public List<String> getIds() {
        return ids;
    }

    public void setIds(List<String> ids) {
        this.ids = ids;
    }

}
