/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.ilsp.xml.CesDoc;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import java.util.List;

/**
 *
 * @author pminos
 */
public class Body {

    @JacksonXmlElementWrapper(useWrapping = false)
    @JacksonXmlProperty(namespace = "", localName = "p")
    private List<P> p;

    public List<P> getP() {
        return p;
    }

    public void setP(List<P> p) {
        this.p = p;
    }
    
    
}
