/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.ilsp.xml.CesDoc;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

/**
 *
 * @author pminos
 */
public class Sofa {
    @JacksonXmlProperty(namespace = "xmi", localName = "id")
    private int id;
    private String sofaNum;
    private String sofaID;
    private String mimeType;
    private String sofaString;

    public Sofa() {
    }

    public String getSofaString() {
        return sofaString;
    }

    public void setSofaString(String sofaString) {
        this.sofaString = sofaString;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSofaNum() {
        return sofaNum;
    }

    public void setSofaNum(String sofaNum) {
        this.sofaNum = sofaNum;
    }

    public String getSofaID() {
        return sofaID;
    }

    public void setSofaID(String sofaID) {
        this.sofaID = sofaID;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    @Override
    public String toString() {
        return "Sofa{" + "sofaString=" + sofaString + '}';
    }

}
