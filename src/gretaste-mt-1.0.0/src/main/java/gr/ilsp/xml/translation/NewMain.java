/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.ilsp.xml.translation;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.dataformat.xml.ser.ToXmlGenerator;
import gr.ilsp.xml.CesDoc.CesDoc;
import java.io.IOException;

/**
 *
 * @author pminos
 */
public class NewMain {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        // TODO code application logic here
        Dictionary d = new Dictionary();
        add(d, "herd", "βότανο");
        add(d, "single portion", "ατομική", "μερίδα");

        XmlMapper xmlMapper = new XmlMapper();
        xmlMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, true);
        xmlMapper.configure(SerializationFeature.INDENT_OUTPUT, true);
        xmlMapper.configure(ToXmlGenerator.Feature.WRITE_XML_DECLARATION, true);
        xmlMapper.writeValue(System.out, d);

    }

    private static void add(Dictionary d, String t, String... g) {
        Unit unit = new Unit();
        d.getUnit().add(unit);
        unit.setTranslation(t);
        for(String s:g) {
            Token tkn = new Token();
            tkn.setValue(s);
            unit.getToken().add(tkn);
        }
    }

}
