/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.ilsp.xml.CesDoc;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import java.util.List;

/**
 *
 * @author pminos
 */
public class P {
    
    @JacksonXmlElementWrapper(useWrapping = false)
    @JacksonXmlProperty(namespace = "", localName = "s")
    private List<S> s;

    public List<S> getS() {
        return s;
    }

    public void setS(List<S> s) {
        this.s = s;
    }
    
    
}
