/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.ilsp.xml.CesDoc;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import java.util.List;

/**
 *
 * @author pminos
 */
@JacksonXmlRootElement(namespace = "", localName = "cesDoc")
public class CesDoc {

    @JacksonXmlProperty(namespace = "xmi", localName = "version")
    private String version;
    @JacksonXmlProperty(namespace = "cas", localName = "Sofa")
    private Sofa sofa;
    @JacksonXmlProperty(namespace = "cas", localName = "NULL")
    private NULL NULL;

    @JacksonXmlProperty(namespace = "tcas", localName = "DocumentAnnotation")
    private DocumentAnnotation documentAnnotation;
    @JacksonXmlProperty(namespace = "types", localName = "Header")
    private Header header;
    @JacksonXmlElementWrapper(useWrapping = false)
    @JacksonXmlProperty(namespace = "types", localName = "Sentence")
    private List<Sentence> sentence;
    @JacksonXmlElementWrapper(useWrapping = false)
    @JacksonXmlProperty(namespace = "types", localName = "POSTag")
    private List<POSTag> postag;
    @JacksonXmlElementWrapper(useWrapping = false)
    @JacksonXmlProperty(namespace = "types", localName = "Lemma")
    private List<Lemma> lemma;

    @JacksonXmlElementWrapper(useWrapping = false)
    @JacksonXmlProperty(namespace = "types", localName = "Token")
    private List<Token> token;
    @JacksonXmlElementWrapper(useWrapping = false)
    @JacksonXmlProperty(namespace = "types", localName = "Paragraph")
    private List<Paragraph> paragraph;
    @JacksonXmlProperty(namespace = "cas", localName = "View")
    private View view;
    @JacksonXmlProperty(namespace = "", localName = "text")
    private Text text;

    public CesDoc() {
    }

    public Text getText() {
        return text;
    }

    public void setText(Text text) {
        this.text = text;
    }

    public Sofa getSofa() {
        return sofa;
    }

    public void setSofa(Sofa sofa) {
        this.sofa = sofa;
    }

    public List<Sentence> getSentence() {
        return sentence;
    }

    public void setSentence(List<Sentence> sentence) {
        this.sentence = sentence;
    }

    public List<POSTag> getPostag() {
        return postag;
    }

    public void setPostag(List<POSTag> postag) {
        this.postag = postag;
    }

    public List<Lemma> getLemma() {
        return lemma;
    }

    public void setLemma(List<Lemma> lemma) {
        this.lemma = lemma;
    }

    public List<Token> getToken() {
        return token;
    }

    public void setToken(List<Token> token) {
        this.token = token;
    }

    @Override
    public String toString() {
        return "XMI{" + "sofa=" + sofa + ", sentence=" + sentence + '}';
    }

    public String getTokenText(Token t) {
        return sofa.getSofaString().substring(t.getBegin(), t.getEnd());
    }

    public String getLemmaText(Token t) {
        for(Lemma l : getLemma()) {
            if(l.getId() == t.getLemma()) 
                return l.getValue();
        }
        return "";
    }

}
