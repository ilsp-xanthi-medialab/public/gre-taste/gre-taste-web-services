/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.ilsp.xml.translation;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

/**
 *
 * @author pminos
 */
public class Token {

    @JacksonXmlProperty(namespace = "", localName = "value", isAttribute=true)
    private String value;
    @JacksonXmlProperty(namespace = "", localName = "lemma", isAttribute=true)
    private boolean lemma=false;
    
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public boolean isLemma() {
        return lemma;
    }

    public void setLemma(boolean lemma) {
        this.lemma = lemma;
    }

    @Override
    public String toString() {
        return value ;
    }

}
