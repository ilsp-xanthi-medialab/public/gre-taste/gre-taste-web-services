/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.ilsp.xml.CesDoc;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

/**
 *
 * @author pminos
 */
public class Token {

    @JacksonXmlProperty(namespace = "xmi", localName = "id")
    private int id;
    private String sofa;

    private int begin;
    private int end;
    private int lemma;
    private int posTag;
    private String orthogr;
    private String tokenType;
    private String sentOrd;

    public Token() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getBegin() {
        return begin;
    }

    public void setBegin(int begin) {
        this.begin = begin;
    }

    public int getEnd() {
        return end;
    }

    public void setEnd(int end) {
        this.end = end;
    }

    public String getSofa() {
        return sofa;
    }

    public void setSofa(String sofa) {
        this.sofa = sofa;
    }

    public int getLemma() {
        return lemma;
    }

    public void setLemma(int lemma) {
        this.lemma = lemma;
    }

    public int getPosTag() {
        return posTag;
    }

    public void setPosTag(int posTag) {
        this.posTag = posTag;
    }

    public String getOrthogr() {
        return orthogr;
    }

    public void setOrthogr(String orthogr) {
        this.orthogr = orthogr;
    }

    public String getTokenType() {
        return tokenType;
    }

    public void setTokenType(String tokenType) {
        this.tokenType = tokenType;
    }

    public String getSentOrd() {
        return sentOrd;
    }

    public void setSentOrd(String sentOrd) {
        this.sentOrd = sentOrd;
    }

    @Override
    public String toString() {
        return "Token{" + "id=" + id + ", sofa=" + sofa + ", begin=" + begin + ", end=" + end + ", lemma=" + lemma + ", posTag=" + posTag + ", orthogr=" + orthogr + ", tokenType=" + tokenType + ", sentOrd=" + sentOrd + '}';
    }

}
