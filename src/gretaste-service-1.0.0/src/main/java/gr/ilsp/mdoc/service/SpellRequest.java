package gr.ilsp.mdoc.service;

/**
 *
 * @author pminos
 */
public class SpellRequest {

    private String content;
    private String result;

    public SpellRequest() {
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

}
