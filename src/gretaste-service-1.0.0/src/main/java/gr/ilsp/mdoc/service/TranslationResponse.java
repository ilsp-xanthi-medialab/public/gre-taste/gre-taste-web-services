/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.ilsp.mdoc.service;

import gr.ilsp.gretaste.mt.TranslatedToken;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author pminos
 */
public class TranslationResponse {

    public List<TranslatedToken> translation = new ArrayList<>();
    public List<String> debug = new ArrayList<>();

    public TranslationResponse() {
    }

    public List<TranslatedToken> getTranslation() {
        return translation;
    }

    public void setTranslation(List<TranslatedToken> translation) {
        this.translation = translation;
    }

    public List<String> getDebug() {
        return debug;
    }

    public void setDebug(List<String> debug) {
        this.debug = debug;
    }

}
