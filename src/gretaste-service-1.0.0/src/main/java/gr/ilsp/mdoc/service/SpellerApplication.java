package gr.ilsp.mdoc.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.support.SpringBootServletInitializer;

/**
 *
 * @author pminos
 */
@SpringBootApplication
public class SpellerApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(SpellerApplication.class, args);
    }

}
