/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.ilsp.mdoc.service;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.dataformat.xml.ser.ToXmlGenerator;
import gr.ilsp.gretaste.json.Entry;
import gr.ilsp.gretaste.json.GEntry;
import gr.ilsp.gretaste.json.Label;
import gr.ilsp.mdoc.speller.dawg.DaciukMihovAutomatonBuilder;
import gr.ilsp.mdoc.speller.dawg.Dawg;
import gr.ilsp.mdoc.speller.dawg.DawgState;
import gr.ilsp.xml.translation.Dictionary;
import gr.ilsp.xml.translation.Token;
import gr.ilsp.xml.translation.Unit;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;
import java.util.logging.Logger;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 *
 * @author pminos
 */
@Service
public class Dictionaries {

    @Value("${model:/opt/gretaste/gretaste.dict}")
    private String model;
    //private RunAutomaton rautomaton;
    Dawg rautomaton;
    Dawg rautomaton_ci;
    private Entry[] entries;
    Map<Integer, Entry> entries_map = new HashMap<Integer, Entry>();

    Map<Integer, ArrayList<GreUnit>> foog_dictionary_el = new HashMap<Integer, ArrayList<GreUnit>>();
    Map<Integer, ArrayList<Unit>> godict;
    Map<Integer, ArrayList<Unit>> general_dictionary_en = new HashMap<Integer, ArrayList<Unit>>();
    Map<Integer, ArrayList<Unit>> general_dictionary_ru = new HashMap<Integer, ArrayList<Unit>>();
    List<MEntry> morphology = new ArrayList<>();
    MEntry[] mentries;

    public Dictionaries() {
        try {
            loadFoodDict();
            loadGeneralDict();
            godict = loadOverrideDict();
            loadMorphology();
        } catch (IOException ex) {
            Logger.getGlobal().info(ex.getMessage());
            Logger.getGlobal().info(ex.toString());
        }
    }

    private void loadMorphology() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mentries = mapper.readValue(FileUtils.readFileToString(new File("/opt/gretaste/morphology.json")), MEntry[].class);
    }

    private void loadFoodDict() throws IOException {
        //Logger.getGlobal().info("loadFoodDict()");
        ObjectMapper mapper = new ObjectMapper();
        entries = mapper.readValue(FileUtils.readFileToString(new File("/opt/gretaste/dictionary.json")), Entry[].class);
        for (Entry e : entries) {
            entries_map.put(e.getId(), e);
            for (Label l : e.getLabels()) {
                l.setLabel(l.getLabel().trim());
            }
        }

        TreeSet<String> set = new TreeSet<>();
        for (Entry e : entries) {
            for (Label l : e.getLabels()) {
                if (l.getLang().equals("el")) {
                    set.add(l.getLabel() + '\0' + e.getId());
                }
            }
        }

        DaciukMihovAutomatonBuilder b = new DaciukMihovAutomatonBuilder();
        DawgState initial = b.create(set.toArray(new String[0]));
        Dawg dawg = new Dawg(initial);
        rautomaton = dawg;

        set.clear();
        for (Entry e : entries) {
            for (Label l : e.getLabels()) {
                if (l.getLang().equals("el")) {
                    set.add(norm(l.getLabel()) + '\0' + e.getId());
                }
            }
        }

        b = new DaciukMihovAutomatonBuilder();
        initial = b.create(set.toArray(new String[0]));
        dawg = new Dawg(initial);
        rautomaton_ci = dawg;

        set.clear();
        for (Entry e : entries) {
            for (Label l : e.getLabels()) {
                if (l.getLang().equals("el")) {
                    GreUnit u = new GreUnit();
                    u.setId(e.getId());
                    String[] tok = l.getLabel().split(" ");
                    for (String x : tok) {
                        if (x == null) {
                            continue;
                        }
                        x = x.trim();
                        if (x.isEmpty()) {
                            continue;
                        }
                        u.getToken().add(x);
                    }
                    int tc = u.getToken().size();
                    if (tc == 0) {
                        return;
                    }
                    if (!foog_dictionary_el.containsKey(tc)) {
                        foog_dictionary_el.put(tc, new ArrayList<>());
                    }
                    foog_dictionary_el.get(tc).add(u);
                }
            }
        }

    }

    private void loadGeneralDict() throws IOException {
        //Logger.getGlobal().info("loadGeneralDict()");
        ObjectMapper mapper = new ObjectMapper();
        GEntry[] gentries = mapper.readValue(FileUtils.readFileToString(new File("/opt/gretaste/dictionary.general.json")), GEntry[].class);
        for (GEntry line : gentries) {
            if (line == null) {
                continue;
            }
            if (line.getSource() == null) {
                continue;
            }
            if (line.getTarget() == null) {
                continue;
            }
            if (line.getLang() == null) {
                continue;
            }

            if ("en".equals(line.getLang())) {
                add(general_dictionary_en, line);
            }
            if ("ru".equals(line.getLang())) {
                add(general_dictionary_ru, line);
            }
        }
    }

    private void add(Map<Integer, ArrayList<Unit>> dict, GEntry line) {

        Unit u = new Unit();
        u.setTranslation(line.getTarget().trim());
        String[] tokens = line.getSource().split(" ");
        for (String t : tokens) {
            t = t.trim();
            if (!t.isEmpty()) {
                Token token = new Token();
                token.setValue(t);
                u.getToken().add(token);
            }
        }
        int l = u.getToken().size();
        if (l == 0) {
            return;
        }
        if (!dict.containsKey(l)) {
            dict.put(l, new ArrayList<>());
        }
        dict.get(l).add(u);
    }

    private Map<Integer, ArrayList<Unit>> loadOverrideDict() throws IOException {
        Logger.getGlobal().info("loadOverrideDict()");
        Map<Integer, ArrayList<Unit>> dict = new HashMap<>();

        XmlMapper xmlMapper = new XmlMapper();
        xmlMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, true);
        xmlMapper.configure(SerializationFeature.INDENT_OUTPUT, true);
        xmlMapper.configure(ToXmlGenerator.Feature.WRITE_XML_DECLARATION, true);
        Dictionary value = xmlMapper.readValue(new File("/opt/gretaste/dict.override.xml"), Dictionary.class);
        for (Unit u : value.getUnit()) {
            int l = u.getToken().size();
            if (l == 0) {
                continue;
            }
            if (!dict.containsKey(l)) {
                dict.put(l, new ArrayList<>());
            }
            dict.get(l).add(u);
        }

        return dict;
    }

    public static String norm(String label) {
        StringBuilder b = new StringBuilder();
        for (char c : label.toCharArray()) {

            switch (c) {
                case 'ά':
                    b.append('α');
                    break;
                case 'έ':
                    b.append('ε');
                    break;
                case 'ί':
                    b.append('ι');
                    break;
                case 'ϊ':
                    b.append('ι');
                    break;
                case 'ΐ':
                    b.append('ι');
                    break;
                case 'ή':
                    b.append('η');
                    break;
                case 'ύ':
                    b.append('υ');
                    break;
                case 'ϋ':
                    b.append('υ');
                    break;
                case 'ΰ':
                    b.append('υ');
                    break;
                case 'ό':
                    b.append('ο');
                    break;
                case 'ώ':
                    b.append('ω');
                    break;

                case 'Ά':
                    b.append('α');
                    break;
                case 'Έ':
                    b.append('ε');
                    break;
                case 'Ί':
                    b.append('ι');
                    break;
                case 'Ϊ':
                    b.append('ι');
                    break;
                case 'Ή':
                    b.append('η');
                    break;
                case 'Ύ':
                    b.append('υ');
                    break;
                case 'Ϋ':
                    b.append('υ');
                    break;
                case 'Ό':
                    b.append('ο');
                    break;
                case 'Ώ':
                    b.append('ω');
                    break;

                case 'ς':
                    b.append('σ');
                    break;

                case 'Α':
                    b.append('α');
                    break;
                case 'Β':
                    b.append('β');
                    break;
                case 'Γ':
                    b.append('γ');
                    break;
                case 'Δ':
                    b.append('δ');
                    break;
                case 'Ε':
                    b.append('ε');
                    break;
                case 'Ζ':
                    b.append('ζ');
                    break;
                case 'Η':
                    b.append('η');
                    break;
                case 'Θ':
                    b.append('θ');
                    break;
                case 'Ι':
                    b.append('ι');
                    break;
                case 'Κ':
                    b.append('κ');
                    break;
                case 'Λ':
                    b.append('λ');
                    break;
                case 'Μ':
                    b.append('μ');
                    break;
                case 'Ν':
                    b.append('ν');
                    break;
                case 'Ξ':
                    b.append('ξ');
                    break;
                case 'Ο':
                    b.append('ο');
                    break;
                case 'Π':
                    b.append('π');
                    break;
                case 'Ρ':
                    b.append('ρ');
                    break;
                case 'Σ':
                    b.append('σ');
                    break;
                case 'Τ':
                    b.append('τ');
                    break;
                case 'Υ':
                    b.append('υ');
                    break;
                case 'Φ':
                    b.append('φ');
                    break;
                case 'Χ':
                    b.append('χ');
                    break;
                case 'Ψ':
                    b.append('ψ');
                    break;
                case 'Ω':
                    b.append('ω');
                    break;

                default:
                    b.append(c);
                    break;
            }

        }
        return b.toString();
    }

}
