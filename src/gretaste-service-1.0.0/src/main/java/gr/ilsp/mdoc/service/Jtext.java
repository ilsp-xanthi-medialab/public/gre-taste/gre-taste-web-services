/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.ilsp.mdoc.service;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author pminos
 */
public class Jtext {

    public String text;
    public String locale;
    public String[] bounds_xy;
    public String corrected;
    public String translated;
    public List<Integer> entityId = new ArrayList<>();
    public boolean isTranslated;
}
