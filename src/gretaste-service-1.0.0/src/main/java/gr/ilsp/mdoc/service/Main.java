/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.ilsp.mdoc.service;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import gr.ilsp.gretaste.json.Entry;
import gr.ilsp.gretaste.json.Label;
import gr.ilsp.gretaste.mt.Span;
import gr.ilsp.gretaste.mt.TranslatedToken;
import gr.ilsp.mdoc.speller.dawg.AutomatonMatcher;
import gr.ilsp.xml.CesDoc.CesDoc;
import gr.ilsp.xml.CesDoc.Token;
import gr.ilsp.xml.translation.Unit;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.attribute.PosixFilePermissions;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author pminos
 */
public class Main {

    private final Dictionaries dictionaries;

    public Main(Dictionaries dictionaries) {
        this.dictionaries = dictionaries;
    }

    public List<TranslatedToken> translateSVC(String text, String locale, boolean transliterate) throws IOException {
        return translateSVC(text, locale, new ArrayList<>(), transliterate);
    }

    public List<TranslatedToken> translateSVC(String text, String locale, List<String> debug, boolean transliterate) throws IOException {
        //test=test.trim();
        //Logger.getGlobal().info("translateSVC()");

        boolean ru = false;
        //Map<String, String> generic_dict = gdict_en;
        if (locale.equals("ru")) {
            //generic_dict = gdict_ru;
            ru = true;
        }
        //Map<Integer, ArrayList<Unit>> odict = godict;

        //String xml = getCached(test, "xceslemma");
        CesDoc value = getCached(text, "xmicas", debug);
        if (value == null) {
            debug.add("error: STOPPING");
            //Logger.getGlobal().info("translateSVC() xml == null");
            return new ArrayList<>();
        }

        List<TranslatedToken> translation = new ArrayList<>();

        //String ltext = "";
        for (Token t : value.getToken()) {
            TranslatedToken tt = new TranslatedToken();
            tt.id = t.getId();
            tt.position = t.getBegin();
            tt.wf = value.getTokenText(t);
            tt.lemma = value.getLemmaText(t);
            // ltext += " " + tt.lemma;
            debug.add(String.format("TranslatedToken(id, position, wf, lemma): %d, %d, %s, %s", tt.id, tt.position, tt.wf, tt.lemma));

            for (MEntry m : dictionaries.mentries) {
                if (tt.wf.equals(m.getW())) {
                    debug.add(String.format("Override Lemma: %s -> %s", tt.lemma, m.getL()));
                    tt.lemma = m.getL();
                    break;
                }
            }

            translation.add(tt);
        }
        // ltext = ltext.trim();
        AutomatonMatcher matcher;
        //matcher = dictionaries.rautomaton.newMatcher(text);

        List<Span> spans = new ArrayList<>();
        //while (matcher.find()) {
        //spans.add(new Span(matcher.start(), matcher.end(), matcher.ids()));
        //scanXML(translation, value, matcher, ru);
        //}
//        matcher = dictionaries.rautomaton.newMatcher(text.toLowerCase(new Locale("el")));
//        //matcher = rautomaton.newMatcher(test);
//        while (matcher.find()) {
//            boolean inside = false;
//            for (Span span : spans) {
//                matcher.group();
//                if (span.contains(matcher.start(), matcher.end())) {
//                    inside = true;
//                    break;
//                }
//            }
//            if (inside) {
//                continue;
//            }
//            spans.add(new Span(matcher.start(), matcher.end(), matcher.ids()));
//            scanXML(translation, value, matcher, ru);
//        }

        matcher = dictionaries.rautomaton_ci.newMatcher(Dictionaries.norm(text));
        //matcher = rautomaton.newMatcher(test);
        while (matcher.find()) {
            boolean inside = false;
            for (Span span : spans) {
                if (span.contains(matcher.start(), matcher.end())) {
                    inside = true;
                    break;
                }
            }
            if (inside) {
                continue;
            }

            Span span = new Span(matcher.start(), matcher.end(), matcher.ids());
            debug.add(String.format("GRETASTE DICT found possible match in text: normalized text: %s (%d,%d), ids: %s", Dictionaries.norm(text).substring(span.getStart(), span.getEnd()), span.getStart(), span.getEnd(), span.getIds().toString()));
            boolean res = scanXML(translation, value, matcher, ru, debug);

            if (res) {
                spans.add(span);
            }
        }
        translateGre(dictionaries.foog_dictionary_el, translation, 1, debug, locale);

        //translate with override dict
        translateOverride(dictionaries.godict, translation, 2, debug);
        if (ru) {
            translateOverride(dictionaries.general_dictionary_ru, translation, 3, debug);
        } else {
            translateOverride(dictionaries.general_dictionary_en, translation, 3, debug);
        }

        for (TranslatedToken tt : translation) {
            if (!tt.translated) {
                if (transliterate) {
                    tt.translation = transliterate(tt.wf);
                    tt.translated = true;
                }
                //tt.translated = false;
            } else {
                //tt.translated = true;
            }
        }
        return translation;

    }

    private boolean scanXML(List<TranslatedToken> translation, CesDoc xml, AutomatonMatcher matcher, boolean ru, List<String> debug) {
        //Logger.getGlobal().info("scanXML()");

        int start = matcher.start();
        int end = matcher.end();
        String group = matcher.group();

        Token s = null, e = null;
        for (Token t : xml.getToken()) {
            if (t.getBegin() == start) {
                s = t;
            }
            if (t.getEnd() == end) {
                e = t;
            }
        }

        if (s == null) {
            debug.add(String.format("GRETASTE DICT: can not find token starting at %d", start));
            return false;
        }
        if (e == null) {
            debug.add(String.format("GRETASTE DICT: can not find token ending at %d", end));
            return false;
        }
        if (s.getBegin() >= e.getEnd()) {
            debug.add(String.format("GRETASTE DICT: start >= end [%d,%d]", s.getBegin(), s.getEnd()));
            return false;
        }

        TranslatedToken first = null;

        int sIdx = xml.getToken().indexOf(s);
        int eIdx = xml.getToken().indexOf(e);
        int sz = eIdx - sIdx;
        sz++;

        for (TranslatedToken tt : translation) {
            if (tt.id >= s.getId() && tt.id <= e.getId()) {
                tt.mwSize = sz;
                tt.translated = true;
                tt.translation = "";
                tt.lexicon = 1;
                for (String sid : matcher.ids()) {
                    tt.entity_ids.add(Integer.parseInt(sid));
                }
                if (tt.id == s.getId()) {
                    first = tt;
                    tt.startOfMW = true;
                    //tt.mw = group;

                    String tlabel = getLabel(tt.entity_ids, ru);
                    if (tlabel == null) {
                        tt.translation = "" + group + "";
                    } else {
                        tt.translation = "" + tlabel + "";
                    }

                    //break;
                } else {
                    tt.partOfMW = true;
                    tt._partOfMW = first;
                }
            }
        }
        return true;
    }

    private void translateOverride(Map<Integer, ArrayList<Unit>> odict, List<TranslatedToken> tt, int dictid, List<String> debug) {
        //Logger.getGlobal().info("translateOverride3()");

        int max = 0;
        for (int k : odict.keySet()) {
            max = Math.max(max, k);
        }
        while (max > 0) {
            if (odict.get(max) != null) {
                if (!odict.get(max).isEmpty()) {
                    _translateOverride(max, odict.get(max), tt, dictid, debug);
                }
            }
            max--;
        }

    }

    private void translateGre(Map<Integer, ArrayList<GreUnit>> odict, List<TranslatedToken> tt, int dictid, List<String> debug, String locale) {
        //Logger.getGlobal().info("translateOverride3()");

        int max = 0;
        for (int k : odict.keySet()) {
            max = Math.max(max, k);
        }
        while (max > 0) {
            if (odict.get(max) != null) {
                if (!odict.get(max).isEmpty()) {
                    _translateGre(max, odict.get(max), tt, dictid, debug, locale);
                }
            }
            max--;
        }

    }

    private void _translateOverride(int max, ArrayList<Unit> dict, List<TranslatedToken> list, int dictid, List<String> debug) {
        for (int i = 0; i < list.size() - (max - 1); i++) {
            List<String> tt_wf = new ArrayList<>();
            List<String> tt_lemma = new ArrayList<>();
            boolean needstr = true;
            for (int j = 0; j < max; j++) {
                tt_wf.add(list.get(i + j).wf);
                tt_lemma.add(list.get(i + j).lemma);
                if (list.get(i + j).translated) {
                    needstr = false;
                }
            }
            if (needstr) {
                //System.out.println("->" + tt);
                String translated = __translate(dict, tt_wf, tt_lemma, debug);
                //System.out.println("->" + translated);
                if (translated != null) {
                    debug.add(String.format("GENERIC DICT %d (size: %d): found match for word,lemma <%s>, <%s>, translation <%s>", dictid, max, tt_wf, tt_lemma, translated));

                    for (int j = 0; j < max; j++) {
                        list.get(i + j).translated = true;
                        list.get(i + j).lexicon = dictid;
                        list.get(i + j).mwSize = max;
                        if (j == 0) {
                            list.get(i + j).translation = translated;
                            list.get(i + j).startOfMW = true;
                        } else {
                            list.get(i + j).translation = "";
                            list.get(i + j).partOfMW = true;
                        }

                    }
                }
            } else {
                //System.out.println(tt);
            }
        }
    }

    private void _translateGre(int max, ArrayList<GreUnit> dict, List<TranslatedToken> list, int dictid, List<String> debug, String locale) {
        for (int i = 0; i < list.size() - (max - 1); i++) {
            List<String> tt_wf = new ArrayList<>();
            List<String> tt_lemma = new ArrayList<>();
            boolean needstr = true;
            for (int j = 0; j < max; j++) {
                tt_wf.add(list.get(i + j).wf);
                tt_lemma.add(list.get(i + j).lemma);
                if (list.get(i + j).translated) {
                    needstr = false;
                }
            }
            if (needstr) {
                //System.out.println("->" + tt);
                List<Integer> translationId = __translateGre(dict, tt_wf, tt_lemma, debug);
                //System.out.println("->" + translated);
                if (translationId != null) {
                    debug.add(String.format("GRE DICT %d (size: %d): found match for word,lemma <%s>, <%s>,  Entities <%s>", dictid, max, tt_wf, tt_lemma, translationId.toString()));

                    String tlabel = getLabel(translationId, locale.equals("ru"));
                    if (tlabel == null) {
                        debug.add(String.format("Entities <%s> has no translation", translationId.toString()));
                    } else {
                        for (int j = 0; j < max; j++) {
                            list.get(i + j).translated = true;
                            list.get(i + j).lexicon = dictid;
                            list.get(i + j).mwSize = max;
                            if (j == 0) {
                                list.get(i + j).startOfMW = true;
                                list.get(i + j).translation = "" + tlabel + "";
                                list.get(i + j).entity_ids.addAll(translationId);
                            } else {
                                list.get(i + j).translation = "";
                                list.get(i + j).partOfMW = true;
                            }

                        }
                    }
                }
            } else {
                //System.out.println(tt);
            }
        }
    }

    private String __translate(ArrayList<Unit> dict, List<String> tt, List<String> ltt, List<String> debug) {

        List<String> m = new ArrayList<>();

        for (Unit u : dict) {
            m.clear();
            for (gr.ilsp.xml.translation.Token t : u.getToken()) {
                m.add(t.getValue());
            }
            if (tt.equals(m)) {
                debug.add(String.format("token match: %s ", tt.toString()));
                return u.getTranslation();
            }
        }
        for (Unit u : dict) {
            m.clear();
            for (gr.ilsp.xml.translation.Token t : u.getToken()) {
                m.add(t.getValue());
            }
            if (ltt.equals(m)) {
                debug.add(String.format("lemma match: %s ", ltt.toString()));
                return u.getTranslation();
            }
        }
        List<String> tt2 = new ArrayList<>();
        for (String s : tt) {
            tt2.add(Dictionaries.norm(s));
        }
        List<String> ltt2 = new ArrayList<>();
        for (String s : ltt) {
            ltt2.add(Dictionaries.norm(s));
        }
        for (Unit u : dict) {
            m.clear();
            for (gr.ilsp.xml.translation.Token t : u.getToken()) {
                m.add(Dictionaries.norm(t.getValue()));
            }
            if (tt2.equals(m)) {
                debug.add(String.format("norm token match: %s ", tt.toString()));
                return u.getTranslation();
            }
        }
        for (Unit u : dict) {
            m.clear();
            for (gr.ilsp.xml.translation.Token t : u.getToken()) {
                m.add(Dictionaries.norm(t.getValue()));
            }
            if (ltt2.equals(m)) {
                debug.add(String.format("norm lemma match: %s ", ltt.toString()));
                return u.getTranslation();
            }
        }

        return null;
    }

    private List<Integer> __translateGre(ArrayList<GreUnit> dict, List<String> tt, List<String> ltt, List<String> debug) {

        List<Integer> r = new ArrayList<>();
        List<String> m = new ArrayList<>();

        for (GreUnit u : dict) {
            m.clear();
            for (String t : u.getToken()) {
                m.add(t);
            }
            if (tt.equals(m)) {
                debug.add(String.format("token match: %s ", tt.toString()));
                r.add(u.getId());
            }
        }

        if (!r.isEmpty()) {
            return r;
        }

        for (GreUnit u : dict) {
            m.clear();
            for (String t : u.getToken()) {
                m.add(t);
            }
            if (ltt.equals(m)) {
                debug.add(String.format("lemma match: %s ", ltt.toString()));
                r.add(u.getId());
            }
        }

        if (!r.isEmpty()) {
            return r;
        }

        List<String> tt2 = new ArrayList<>();
        for (String s : tt) {
            tt2.add(Dictionaries.norm(s));
        }
        List<String> ltt2 = new ArrayList<>();
        for (String s : ltt) {
            ltt2.add(Dictionaries.norm(s));
        }
        for (GreUnit u : dict) {
            m.clear();
            for (String t : u.getToken()) {
                m.add(t);
            }
            if (tt2.equals(m)) {
                debug.add(String.format("norm token match: %s ", tt.toString()));
                r.add(u.getId());
            }
        }

        if (!r.isEmpty()) {
            return r;
        }
        for (GreUnit u : dict) {
            m.clear();
            for (String t : u.getToken()) {
                m.add(t);
            }
            if (ltt2.equals(m)) {
                debug.add(String.format("norm lemma match: %s ", ltt.toString()));
                r.add(u.getId());
            }
        }
        if (!r.isEmpty()) {
            return r;
        }
        return null;
    }

    public String list2String(List<TranslatedToken> translation) {
        StringBuilder b = new StringBuilder();
        for (TranslatedToken tt : translation) {
            if (tt.translated) {
                b.append(tt.translation).append(" ");
            } else {
                b.append(tt.wf).append(" ");
            }
        }
        b.append("\n");
        return b.toString();
    }

    //select a translation, ignore untranslated entities
    private String getLabel(List<Integer> entity_ids, boolean ru) {
        List<Entry> matched_entries = new ArrayList<>();
        List<String> possible_translations = new ArrayList<>();
        for (Integer entry : entity_ids) {

            matched_entries.add(dictionaries.entries_map.get(entry));

        }
        for (Entry entry : matched_entries) {
            String l = getLabel(entry, ru);
            if (l == null) {
                continue;
            }
            l = l.trim();
            if (l.isEmpty()) {
                continue;
            }
            if (possible_translations.isEmpty()) {
                entity_ids.remove((Integer) entry.getId());
                entity_ids.add(0, (Integer) entry.getId());
            }
            possible_translations.add(l);
        }
        if (possible_translations.isEmpty()) {
            return null;
        }
        return possible_translations.get(0);
    }

    //return the translation
    private String getLabel(Entry entry, boolean ru) {
        String locale = "en";
        if (ru) {
            locale = "ru";
        }
        for (Label l : entry.getLabels()) {
            if (l.getLang().equals(locale)) {
                if (l.getType().equals("Προτιμώμενος")) {
                    return l.getLabel();
                }
            }
        }
        for (Label l : entry.getLabels()) {
            if (l.getLang().equals(locale)) {
                if (l.getType().equals("Συνώνυμος")) {
                    return l.getLabel();
                }
            }
        }
        for (Label l : entry.getLabels()) {
            if (l.getLang().equals(locale)) {
                if (l.getType().equals("Κρυφός")) {
                    return l.getLabel();
                }
            }
        }
        return null;
    }

    //load analysis from cache, call ILSP web service if not in cache
    private CesDoc getCached(String input, String type, List<String> debug) throws IOException {

        XmlMapper xmlMapper = new XmlMapper();
        xmlMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, true);

        String md5 = DigestUtils.md5Hex(input).toUpperCase();
        //Logger.getGlobal().info("getCached(" + md5 + ") size:" + input.length());
        File cache = new File("/opt/gretaste/.cache");
        cache.mkdir();
        File xml = new File(cache, md5 + "." + type);
        if (xml.exists()) {

            String content = FileUtils.readFileToString(xml);
            if (!content.isEmpty()) {
                CesDoc value;
                try {
                    value = xmlMapper.readValue(content, CesDoc.class);
                    debug.add("ILSP Service: FROM CACHE");
                    return value;
                } catch (Throwable t) {
                    xml.delete();
                }
            }
        }

        CesDoc value = null;
        String content = get(input, type);
        if (content == null) {
            Logger.getGlobal().info("content == null");
            debug.add("ILSP Service: ERROR");
        } else {
            try {
                value = xmlMapper.readValue(content, CesDoc.class);
                FileUtils.write(xml, content);
                debug.add("ILSP Service: SUCCESS");
                return value;
            } catch (Throwable t) {
            }
        }

//        if (false) {
//            content = getURL(input, type);
//            if (content == null) {
//                Logger.getGlobal().info("content == null");
//            } else {
//                try {
//                    value = xmlMapper.readValue(content, CesDoc.class);
//                    FileUtils.write(xml, content);
//                    return value;
//                } catch (Throwable nt) {
//                }
//            }
//        }
        return value;
    }

    //call ILSP web service
    private String get(String input, String type) {

        Logger.getGlobal().info("calling ILSP WEB SERVICE");
        java.lang.String inputType = "txt";
        java.lang.String outputType = type;
        java.lang.String inputDirectData = input;
        java.lang.String inputUrl = "";
        java.lang.String inputEncoding = "UTF-8";
        java.lang.String language = "el";
        java.lang.Boolean inputIsURLlist = Boolean.FALSE;
        javax.xml.ws.Holder<java.lang.String> report = new javax.xml.ws.Holder<java.lang.String>();
        javax.xml.ws.Holder<java.lang.Long> detailedStatus = new javax.xml.ws.Holder<java.lang.Long>();
        javax.xml.ws.Holder<java.lang.String> output = new javax.xml.ws.Holder<java.lang.String>();
        javax.xml.ws.Holder<java.lang.String> outputUrl = new javax.xml.ws.Holder<java.lang.String>();
        javax.xml.ws.Holder<java.lang.String> outputList = new javax.xml.ws.Holder<java.lang.String>();
        javax.xml.ws.Holder<java.lang.String> outputListUrl = new javax.xml.ws.Holder<java.lang.String>();
        javax.xml.ws.Holder<byte[]> outputArchive = new javax.xml.ws.Holder<byte[]>();
        javax.xml.ws.Holder<java.lang.String> outputArchiveUrl = new javax.xml.ws.Holder<java.lang.String>();
        try {
            Logger.getGlobal().info("calling ILSP WEB SERVICE: 2");
            org.soaplab.getstarted.ilsp_nlp.IlspNlpService service = new org.soaplab.getstarted.ilsp_nlp.IlspNlpService();
            Logger.getGlobal().info("calling ILSP WEB SERVICE: 3");
            org.soaplab.getstarted.ilsp_nlp.IlspNlp port = service.getIlspNlpPort();
            Logger.getGlobal().info("calling ILSP WEB SERVICE: 4");
            port.runAndWaitFor(inputType, outputType, inputDirectData, inputUrl, inputEncoding, language, inputIsURLlist, report, detailedStatus, output, outputUrl, outputList, outputListUrl, outputArchive, outputArchiveUrl);
            Logger.getGlobal().info("calling ILSP WEB SERVICE: 5");
            if (detailedStatus.value != null) {
                Logger.getGlobal().info(detailedStatus.value.toString());
                //FileUtils.write(new File(".cache/detailedStatus"), detailedStatus.value.toString(), "UTF-8");
            }
            if (output.value == null) {
                Logger.getGlobal().info("ILSP WEB SERVICE error");
                if (report.value != null) {
                    Logger.getGlobal().info("ILSP WEB SERVICE error: " + report.value);
                }
            }
            return output.value;
        } catch (Exception ex) {
            // TODO handle custom exceptions here
            //System.err.println("This parrot is no more!");
            Logger.getGlobal().info("This parrot is no more!");
            Logger.getGlobal().info(ex.getMessage());
            throw new RuntimeException(ex);
        } catch (Throwable ex) {
            // TODO handle custom exceptions here
            //System.err.println("This parrot is no more!");
            Logger.getGlobal().info("This parrot is no more!");
            Logger.getGlobal().info(ex.getMessage());
            throw new RuntimeException(ex);
        }
    }

    //call ILSP web service
    private String getURL(String input, String type) {
        String md5 = DigestUtils.md5Hex(input).toUpperCase();
        String md5ext = md5 + ".txt";
        File cache = new File("/var/www/ilsp.cb");
        cache.mkdir();
        File xml = new File(cache, md5ext);
        try {
            FileUtils.write(xml, input);
            Files.setPosixFilePermissions(xml.toPath(), PosixFilePermissions.fromString("rw-r--r--"));

        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }

        Logger.getGlobal().info("calling ILSP WEB SERVICE");
        java.lang.String inputType = "txt";
        java.lang.String outputType = type;
        //java.lang.String inputDirectData = input;
        java.lang.String inputDirectData = null;
        java.lang.String inputUrl = "http://gretaste.ilsp.gr/ilsp.cb/" + md5ext;
        java.lang.String inputEncoding = "UTF-8";
        java.lang.String language = "el";
        java.lang.Boolean inputIsURLlist = Boolean.FALSE;
        javax.xml.ws.Holder<java.lang.String> report = new javax.xml.ws.Holder<java.lang.String>();
        javax.xml.ws.Holder<java.lang.Long> detailedStatus = new javax.xml.ws.Holder<java.lang.Long>();
        javax.xml.ws.Holder<java.lang.String> output = new javax.xml.ws.Holder<java.lang.String>();
        javax.xml.ws.Holder<java.lang.String> outputUrl = new javax.xml.ws.Holder<java.lang.String>();
        javax.xml.ws.Holder<java.lang.String> outputList = new javax.xml.ws.Holder<java.lang.String>();
        javax.xml.ws.Holder<java.lang.String> outputListUrl = new javax.xml.ws.Holder<java.lang.String>();
        javax.xml.ws.Holder<byte[]> outputArchive = new javax.xml.ws.Holder<byte[]>();
        javax.xml.ws.Holder<java.lang.String> outputArchiveUrl = new javax.xml.ws.Holder<java.lang.String>();
        try {
            Logger.getGlobal().info("calling ILSP WEB SERVICE: 2");
            org.soaplab.getstarted.ilsp_nlp.IlspNlpService service = new org.soaplab.getstarted.ilsp_nlp.IlspNlpService();
            Logger.getGlobal().info("calling ILSP WEB SERVICE: 3");
            org.soaplab.getstarted.ilsp_nlp.IlspNlp port = service.getIlspNlpPort();
            Logger.getGlobal().info("calling ILSP WEB SERVICE: 4");
            port.runAndWaitFor(inputType, outputType, inputDirectData, inputUrl, inputEncoding, language, inputIsURLlist, report, detailedStatus, output, outputUrl, outputList, outputListUrl, outputArchive, outputArchiveUrl);
            Logger.getGlobal().info("calling ILSP WEB SERVICE: 5");
            if (detailedStatus.value != null) {
                Logger.getGlobal().info(detailedStatus.value.toString());
                //FileUtils.write(new File(".cache/detailedStatus"), detailedStatus.value.toString(), "UTF-8");
            }
            if (output.value == null) {
                Logger.getGlobal().info("ILSP WEB SERVICE error");
                if (report.value != null) {
                    Logger.getGlobal().info("ILSP WEB SERVICE error: " + report.value);
                }
            }
            return output.value;
        } catch (Exception ex) {
            // TODO handle custom exceptions here
            //System.err.println("This parrot is no more!");
            Logger.getGlobal().info("This parrot is no more!");
            Logger.getGlobal().info(ex.getMessage());
            throw new RuntimeException(ex);
        } catch (Throwable ex) {
            // TODO handle custom exceptions here
            //System.err.println("This parrot is no more!");
            Logger.getGlobal().info("This parrot is no more!");
            Logger.getGlobal().info(ex.getMessage());
            throw new RuntimeException(ex);
        }
    }

    public static void main(String[] args) {
        System.out.println(transliterate("ρεύομαι"));
        System.out.println(transliterate("αυτώ"));
        System.out.println(transliterate("μπαμ"));
        System.out.println(transliterate("μπαμπης"));
    }

    static String transliterate(String w) {
        w = w.toLowerCase();
//        w = w.replace("αι", "ai");
//        w = w.replace("αί", "ai");
//        w = w.replace("οι", "oi");
//        w = w.replace("οί", "oi");
        w = w.replace("ου", "ou");
        w = w.replace("ού", "ou");
//        w = w.replace("ει", "ei");
//        w = w.replace("εί", "ei");

        w = w.replaceAll("([αβγδεζηλιmμνορω][αεη])[υύ]", "$1v");
        w = w.replaceAll("([αεη])[υύ]", "$1f");

        w = w.replace("ντ", "nt");
        w = w.replaceAll("^μπ", "b");
        w = w.replaceAll("μπ$", "b");
        w = w.replace("μπ", "mp");

        w = w.replace("τσ", "ts");
        w = w.replace("τζ", "tz");
        w = w.replace("γγ", "ng");
        //w = w.replace("γκ", "gk");
        w = w.replace("γχ", "nch");
        w = w.replace("γξ", "nx");
        w = w.replace("θ", "th");
        w = w.replace("χ", "ch");
        w = w.replace("ψ", "ps");

        String g = "αάβγδεέζηήιίϊΐκλμνξοόπρσςτυύϋΰφωώ";
        String e = "aavgdeeziiiiiiklmnxooprsstyyyyfoo";
        for (int i = 0; i < g.length(); i++) {
            w = w.replace(g.charAt(i), e.charAt(i));
        }
        return w;

    }

}
