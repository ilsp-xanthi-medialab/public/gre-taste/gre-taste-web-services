package gr.ilsp.mdoc.service;

import gr.ilsp.gretaste.mt.TranslatedToken;
import gr.ilsp.mdoc.speller.MdocSpeller;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author pminos
 */
@CrossOrigin(origins = "*")
@RestController
public class SpellerService {

    @Value("${model:/opt/gretaste/gretaste.dict}")
    private String model;
    @Autowired
    private Dictionaries dictionaries;

    @PostConstruct
    public void init() {
    }

    //curl -X POST -H "Content-Type: application/json" -d '{ "content":"ειμε πολυ καλυτερα"}' http://gretaste.ilsp.gr/gretaste-service/check
    //curl -X POST -H "Content-Type: application/json" --data-binary @json  http://localhost:8088/mdoc-service/check
    @RequestMapping(value = {"/check"}, method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public SpellRequest checkJson(@RequestBody SpellRequest r) {
        MdocSpeller mspeller = new MdocSpeller(model);
        r.setResult(mspeller.autocorrect(r.getContent()));
        return r;
    }

    //curl -X POST -H "Content-Type: text/plain" --data-binary @txt http://localhost:8088/mdoc-service/check
    @RequestMapping(value = {"/check"}, method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.TEXT_PLAIN_VALUE)
    public SpellRequest checkText(@RequestBody String s) {
        SpellRequest r = new SpellRequest();
        r.setContent(s);
        return checkJson(r);
    }

    //curl -X POST -H "Content-Type: application/json" --data-binary @data.json  http://localhost:8088/mdoc-service/checkjson
    //curl -X POST -H "Content-Type: application/json" --data-binary @data.json  http://gretaste.ilsp.gr/gretaste-service/checkjson?locale=el
    @RequestMapping(value = {"/checkjson"}, method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Request checkText(@RequestParam(name = "locale", required = false) String locale, @RequestBody Request s) {
        MdocSpeller mspeller = new MdocSpeller(model);
        for (Jtext item : s.items) {
            item.corrected = mspeller.autocorrect(item.text);
            if (item.corrected != null) {
                item.corrected = item.corrected.trim();
            }
        }

        //r.setResult(mspeller.autocorrect(r.getContent()));
        return s;
    }

    //curl -X POST -H "Content-Type: application/json" --data-binary @data.json  http://localhost:8081/gretaste-service/translatejson
    //curl -X POST -H "Content-Type: application/json" --data-binary @data.json  http://gretaste.ilsp.gr/gretaste-service/translatejson?locale=el
    @RequestMapping(value = {"/translatejson"}, method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Request translateText(@RequestParam(name = "locale", required = false) String locale,
            @RequestParam(name = "spell", required = false) String spell,
            @RequestParam(name = "translate", required = false) String translate,
            @RequestBody Request s) {

        if (locale == null) {
            locale = "en";
        }

        if (spell != null) {
            MdocSpeller mspeller = new MdocSpeller(model);
            for (Jtext item : s.items) {
                item.corrected = mspeller.autocorrect(item.text);
                if (item.corrected != null) {
                    item.corrected = item.corrected.trim();
                } else {
                    item.corrected = item.text;
                }
            }
        }
        if (translate != null) {
            Main m = new Main(dictionaries);

            String text = "";
            List<Integer> offset = new ArrayList<>();

            for (int i = 0; i < s.items.size(); i++) {
                if (i > 0) {
                    text += " ";
                }
                offset.add(text.length());
                Jtext item = s.items.get(i);

                if (spell == null) {
                    text += item.text;
                } else {
                    text += item.corrected;
                }
            }
            offset.add(text.length());
            try {
                List<TranslatedToken> xtranslation = m.translateSVC(text, locale, true);

                for (int i = 0; i < xtranslation.size(); i++) {
                    TranslatedToken tt = xtranslation.get(i);
                    if (tt.startOfMW) {
                        split(xtranslation, i);
                    }
                }

                for (TranslatedToken tt : xtranslation) {
                    int idx = index(tt, offset);
                    Jtext item = s.items.get(idx);
                    if (tt.startOfMW) {
                        item.entityId.addAll(tt.entity_ids);
                    }
                    if (tt.partOfMW) {
                        item.entityId.addAll(tt._partOfMW.entity_ids);
                    }
                    item.isTranslated = tt.translated;
                    if (item.translated == null) {
                        item.translated = "";
                        //item.isTranslated = false;
                    } else {
                        //item.isTranslated = true;
                    }
                    if (tt.translation != null) {
                        item.translated = (item.translated + " " + tt.translation).trim();
                    }
                }
                //m.list2String(xtranslation);
                //System.out.println(xtranslation);
            } catch (IOException ex) {
                Logger.getGlobal().info(ex.getMessage());

                Logger.getLogger(SpellerService.class.getName()).log(Level.SEVERE, null, ex);
            }

//            for (Jtext item : s.items) {
//                try {
//
//                    List<TranslatedToken> translation;
//
//                    if (spell == null) {
//                        translation = m.translateSVC(item.text, locale);
//                    } else {
//                        translation = m.translateSVC(item.corrected, locale);
//                    }
//
//                    item.translated = m.list2String(translation);
//                    for (TranslatedToken t : translation) {
//                        //item.entityId.add(t.entity_id);
//                    }
//                    if (item.translated != null) {
//                        item.translated = item.translated.trim();
//                    }
//                } catch (IOException ex) {
//                    Logger.getLogger(SpellerService.class.getName()).log(Level.SEVERE, null, ex);
//                }
//            }
        }
        //r.setResult(mspeller.autocorrect(r.getContent()));
        return s;
    }

    //curl -X POST -H "Content-Type: text/plain" --data-binary @txt http://localhost:8081/gretaste-service/translate
    //curl -X POST -H "Content-Type: text/plain" --data-binary @txt http://gretaste.ilsp.gr/gretaste-service/translate
    //curl -X POST -H "Content-Type: text/plain" --data-binary @txt http://gretaste.ilsp.gr/gretaste-service/translate
    @RequestMapping(value = {"/translate"}, method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.TEXT_PLAIN_VALUE)
    public SpellRequest translate(@RequestParam(name = "locale", required = false) String locale, @RequestBody String s) {
        if (locale == null) {
            locale = "en";
        }

        Main m = new Main(dictionaries);
        SpellRequest r = new SpellRequest();
        try {
            r.setContent(s);
            List<TranslatedToken> translation = m.translateSVC(s, locale, true);
            r.setResult(m.list2String(translation));
        } catch (IOException ex) {
            Logger.getLogger(SpellerService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return (r);
    }

    @RequestMapping(value = {"/translateget"}, method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public List<TranslatedToken> translateget(@RequestParam(name = "locale", required = false) String locale,
            @RequestParam(name = "spell", required = false) String spell,
            @RequestParam(name = "text", required = false) String s) {

        if (locale == null) {
            locale = "en";
        }
        if (spell != null) {
            MdocSpeller mspeller = new MdocSpeller(model);

            String corrected = mspeller.autocorrect(s);
            if (corrected != null) {
                corrected = corrected.trim();
            } else {
                corrected = s;
            }
            s = corrected;
        }

        Main m = new Main(dictionaries);
        SpellRequest r = new SpellRequest();
        try {
            r.setContent(s);
            List<TranslatedToken> translation = m.translateSVC(s, locale, true);
            //r.setResult(m.list2String(translation));
            return translation;
        } catch (IOException ex) {
            Logger.getLogger(SpellerService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return new ArrayList<>();
    }

    @RequestMapping(value = {"/translate2"}, method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.TEXT_PLAIN_VALUE)
    public TranslationResponse translate2(@RequestParam(name = "locale", required = false) String locale,
            @RequestParam(name = "spell", required = false) String spell,
            @RequestParam(name = "notransliterate", required = false) String notransliterate,
            @RequestBody String s) {

        TranslationResponse tr = new TranslationResponse();

        boolean transliterate = true;
        if (locale == null) {
            locale = "en";
        }
        if (notransliterate != null) {
            transliterate = false;
        }
        if (spell != null) {
            if (g_mspeller == null) {
                g_mspeller = new MdocSpeller(model);
            }

            String corrected = g_mspeller.autocorrect(s);
            if (corrected != null) {
                corrected = corrected.trim();

                if (!s.trim().equals(corrected)) {
                    tr.debug.add(String.format("autocorrect input: %s", s));
                    tr.debug.add(String.format("autocorrect result:  %s", corrected));
                }
                s = corrected;
            }
        }

        Main m = new Main(dictionaries);
        try {
            tr.translation = m.translateSVC(s, locale, tr.debug, transliterate);
            //r.setResult(m.list2String(translation));
            return tr;
        } catch (IOException ex) {
            Logger.getLogger(SpellerService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return tr;
    }

    private MdocSpeller g_mspeller;

    @RequestMapping(value = {"/spell2"}, method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.TEXT_PLAIN_VALUE)
    public List<String[]> spell2(@RequestBody String s) {

        try {
            if (g_mspeller == null) {
                g_mspeller = new MdocSpeller(model);
            }

            List<String[]> ret = new ArrayList<>();
            String corrected = g_mspeller.autocorrect(s, ret);
            if (corrected != null) {
                corrected = corrected.trim();
            } else {
                corrected = s;
            }
            s = corrected;
            return ret;
        } catch (Throwable ex) {
            Logger.getLogger(SpellerService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return new ArrayList<>();
    }

    @RequestMapping(value = {"/spell2get"}, method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public List<String[]> spell2get(@RequestParam(name = "text", required = false) String s) {

        try {
            MdocSpeller mspeller = new MdocSpeller(model);

            List<String[]> ret = new ArrayList<>();
            String corrected = mspeller.autocorrect(s, ret);
            if (corrected != null) {
                corrected = corrected.trim();
            } else {
                corrected = s;
            }
            s = corrected;
            return ret;
        } catch (Throwable ex) {
            Logger.getLogger(SpellerService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return new ArrayList<>();
    }

    private int index(TranslatedToken tt, List<Integer> offset) {
        for (int i = 0; i < offset.size() - 1; i++) {
            if (tt.position >= offset.get(i) && tt.position < offset.get(i + 1)) {
                return i;
            }
        }
        return 0;

    }

    private void split(List<TranslatedToken> xtranslation, int curIdx) {
        TranslatedToken tt = xtranslation.get(curIdx);
        int sz = tt.mwSize;
        String[] _parts = tt.translation.split(" ");
        List<String> parts = new ArrayList<>();
        for (String s : _parts) {
            s = s.trim();
            if (s.isEmpty()) {
                continue;
            }
            parts.add(s);
        }
        for (int xi = 0; xi < sz; xi++) {
            Logger.getGlobal().info(xtranslation.get(curIdx + xi).wf);
        }
        Logger.getGlobal().info("\t" + parts.toString());
        int tsz = parts.size();
        if (sz == 1 && tsz == 1) {
            return;
        }

        if (sz == tsz) {
            for (int j = 0; j < sz; j++) {
                xtranslation.get(curIdx + j).translation = parts.get(j);
            }
        } else if (sz > tsz) {
            for (int j = 0; j < sz; j++) {
                if (j < tsz) {
                    xtranslation.get(curIdx + j).translation = parts.get(j);
                } else {
                    xtranslation.get(curIdx + j).translation = "";
                }
            }
        } else {
            for (int j = 0; j < sz; j++) {
                xtranslation.get(curIdx + j).translation = parts.get(j);
            }

            for (int k = sz; k < tsz; k++) {
                xtranslation.get(curIdx + sz - 1).translation += " ";
                xtranslation.get(curIdx + sz - 1).translation += parts.get(k);
            }

        }

    }
}
